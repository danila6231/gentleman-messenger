require 'serverspec'

# Operating System Requirements
if os[:family] == 'ubuntu' && os[:release] == '18.04'
  describe package('ubuntu-release-upgrader-core') do
    it { should be_installed }
  end
elsif os[:family] == 'darwin' && os[:release] == '19.0.0'
  describe command('sw_vers -productVersion') do
    its(:stdout) { should match /10\.15\.\d+/ }
  end
end

# Ansible Requirements
describe command('ansible --version') do
  its(:stdout) { should match /ansible \[core 2\.1[4-9]|[2-9][0-9]*\.[0-9]+(\.[0-9]+)\]*/ }
end

# Docker Requirements
describe command('docker --version') do
  its(:stdout) { should match /Docker version [5-9]|1[0-9]|2[0-9]\.[0-9]+\.([3-9]|[1-9][0-9]+)+/ }
end

# Docker Compose Requirements
describe command('docker-compose --version') do
  its(:stdout) { should match /Docker compose version v[1-3]\.2[9-9]|[3-9][0-9]*\.[0-9]+(\.[0-9]+)*/ }
end

# sshpass Requirements
describe package('sshpass') do
  it { should be_installed }
end

# Python Requirements
describe command('python3 --version') do
  its(:stdout) { should match /Python 3\.9|1[0-9]+\.\d+/ }
end
