from sqlalchemy.ext.asyncio import AsyncEngine

from aiohttp import web

from messenger.exceptions import ChatNotFound, DatabaseConnectionError, ForbiddenTask

from messenger.features.cache import (
    get_messages_cached,
    cache_response,
    invalidate_cache,
)

from messenger.features.user_control import user_control

from messenger.validation import (
    CreateChatBody,
    CreateChatQueryParams,
    AddUserBody,
    AddUserPathParams,
    AddUserQueryParams,
    GetMessagesPathParams,
    GetMessagesQueryParams,
    SendMessageBody,
    SendMessagePathParams,
    SendMessageQueryParams,
)

from messenger.handlers.alternative_handler import AlternativeResponse

from messenger.handlers.db_handler import DatabaseResponse

from messenger.db.db_init import connect_to_db


class Handler:
    def __init__(self):
        pass

    @staticmethod
    async def create_chat(request: web.Request) -> web.Response:
        connect_to_db(request.app)
        data = await request.text()
        body = CreateChatBody.parse_raw(data)
        query_params = CreateChatQueryParams.parse_obj(request.query)
        user_control(request.app["user_control"], query_params.username)
        engine = request.app["engine"]
        resp = await DatabaseResponse.create_chat(engine, body, query_params)
        return web.json_response(resp, status=201)

    @staticmethod
    async def add_user(request: web.Request) -> web.Response:
        connect_to_db(request.app)
        path_params = AddUserPathParams.parse_obj(request.match_info)
        query_params = AddUserQueryParams.parse_obj(request.query)
        data = await request.text()
        body = AddUserBody.parse_raw(data)
        user_control(request.app["user_control"], query_params.username)
        engine = request.app["engine"]
        resp = await DatabaseResponse.add_user(engine, body, path_params, query_params)
        return web.json_response(resp, status=201)

    @staticmethod
    async def get_messages(request: web.Request) -> web.Response:
        path_params = GetMessagesPathParams.parse_obj(request.match_info)
        query_params = GetMessagesQueryParams.parse_obj(request.query)
        cache_resp = get_messages_cached(
            request.app["cache"], path_params, query_params
        )
        user_control(request.app["user_control"], query_params.username)
        if cache_resp is not None:
            return cache_resp
        try:
            connect_to_db(request.app)
            engine = request.app["engine"]
            resp = await DatabaseResponse.get_messages(
                engine, path_params, query_params
            )
            cache_response(request.app["cache"], path_params, query_params, resp)
        except ChatNotFound:
            raise
        except DatabaseConnectionError:
            resp = AlternativeResponse.get_messages(path_params, query_params)
        return web.json_response(resp, status=200)

    @staticmethod
    async def send_message(request: web.Request) -> web.Response:
        connect_to_db(request.app)
        path_params = SendMessagePathParams.parse_obj(request.match_info)
        query_params = SendMessageQueryParams.parse_obj(request.query)
        data = await request.text()
        body = SendMessageBody.parse_raw(data)
        user_control(request.app["user_control"], query_params.username)
        engine = request.app["engine"]
        resp = await DatabaseResponse.send_message(
            engine, body, path_params, query_params
        )
        invalidate_cache(request.app["cache"], path_params.chat_id)
        return web.json_response(resp, status=201)

    @staticmethod
    async def ping_db(request: web.Request) -> web.Response:
        connect_to_db(request.app)
        engine: AsyncEngine = request.app["engine"]
        try:
            async with engine.connect() as conn:
                pass
        except Exception as e:
            raise DatabaseConnectionError
        return web.json_response({"message": "success"}, status=200)

    @staticmethod
    async def ping(request: web.Request) -> web.Response:
        return web.json_response({"message": "success"}, status=200)
