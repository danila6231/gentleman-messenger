from sqlalchemy.ext.asyncio import AsyncConnection, AsyncEngine


from messenger.exceptions import (
    ChatNotFound,
    UserInChatNotFound,
    UserNotFound,
    TaskNotFound,
)

from messenger.db.db_queries import DatabaseQuery

from messenger.validation import (
    CreateChatBody,
    CreateChatQueryParams,
    AddUserBody,
    AddUserPathParams,
    AddUserQueryParams,
    GetMessagesPathParams,
    GetMessagesQueryParams,
    SendMessageBody,
    SendMessagePathParams,
    SendMessageQueryParams,
    GetSearchMessagesPathParams,
    GetSearchMessagesQueryParams,
    StartSearchBody,
    StartSearchQueryParams,
    SearchStatusPathParams,
)


async def chat_exists(chat_id: str, conn: AsyncConnection) -> None:
    resp = await conn.execute(DatabaseQuery.check_chat_exists(int(chat_id)))
    if len(resp.all()) == 0:
        raise ChatNotFound


async def user_in_chat_exists(
    chat_id: str, user_id: str, conn: AsyncConnection
) -> None:
    resp = await conn.execute(
        DatabaseQuery.check_user_in_chat_exists(int(chat_id), int(user_id))
    )
    if len(resp.all()) == 0:
        raise UserInChatNotFound


async def user_exists(user_name: str, conn: AsyncConnection) -> None:
    resp = await conn.execute(DatabaseQuery.check_user_exists(user_name))
    if len(resp.all()) == 0:
        raise UserNotFound


async def task_exists(task_id: str, conn: AsyncConnection) -> None:
    resp = await conn.execute(DatabaseQuery.check_task_exists(int(task_id)))
    if len(resp.all()) == 0:
        raise TaskNotFound


class DatabaseResponse:
    def __init__(self):
        pass

    @staticmethod
    async def create_chat(
        engine: AsyncEngine, body: CreateChatBody, query_params: CreateChatQueryParams
    ) -> dict:
        async with engine.begin() as conn:
            result = await conn.execute(DatabaseQuery.create_chat(body.chat_name))
            chat_id = result.returned_defaults[0]
        return {"chat_id": str(chat_id)}

    @staticmethod
    async def add_user(
        engine: AsyncEngine,
        body: AddUserBody,
        path_params: AddUserPathParams,
        query_params: AddUserQueryParams,
    ) -> dict:
        async with engine.begin() as conn:
            await chat_exists(path_params.chat_id, conn)
            await conn.execute(DatabaseQuery.add_user_data(body.user_name))
            result = await conn.execute(
                DatabaseQuery.add_user(body.user_name, int(path_params.chat_id))
            )
            user_id = result.returned_defaults[0]
        return {"user_id": str(user_id)}

    @staticmethod
    async def get_messages(
        engine: AsyncEngine,
        path_params: GetMessagesPathParams,
        query_params: GetMessagesQueryParams,
    ) -> dict:
        async with engine.begin() as conn:
            await chat_exists(path_params.chat_id, conn)
            result = await conn.execute(
                DatabaseQuery.get_messages(
                    int(path_params.chat_id), query_params.limit, query_params.from_
                )
            )
        resp = {
            "messages": [],
            "iterator": str(query_params.from_ + query_params.limit),
        }
        rows = result.all()
        for row in rows:
            resp["messages"].append({"text": row["text"]})
        return resp

    @staticmethod
    async def send_message(
        engine: AsyncEngine,
        body: SendMessageBody,
        path_params: SendMessagePathParams,
        query_params: SendMessageQueryParams,
    ) -> dict:
        async with engine.begin() as conn:
            await chat_exists(path_params.chat_id, conn)
            await user_in_chat_exists(path_params.chat_id, query_params.user_id, conn)
            result = await conn.execute(
                DatabaseQuery.send_message(
                    int(query_params.user_id), int(path_params.chat_id), body.message
                )
            )
            message_id = result.returned_defaults[0]
        return {"message_id": str(message_id)}

    @staticmethod
    async def add_task(
        engine: AsyncEngine,
        body: StartSearchBody,
        query_params: StartSearchQueryParams,
    ) -> dict:
        async with engine.begin() as conn:
            await user_exists(query_params.username, conn)
            result = await conn.execute(
                DatabaseQuery.add_task(query_params.username, body.message)
            )
            task_id = result.returned_defaults[0]
        return {"task_id": str(task_id)}

    @staticmethod
    async def get_task_status(
        engine: AsyncEngine, path_params: SearchStatusPathParams
    ) -> dict:
        async with engine.begin() as conn:
            result = await conn.execute(
                DatabaseQuery.check_task_exists(
                    int(path_params.task_id),
                )
            )
            row = result.one()
            if not row:
                raise TaskNotFound
            status = row["status"].value
        return {"status": status}

    @staticmethod
    async def check_task_allowed(
        engine: AsyncEngine,
        path_params: GetSearchMessagesPathParams,
        query_params: GetSearchMessagesQueryParams,
    ) -> bool:
        async with engine.begin() as conn:
            result = await conn.execute(
                DatabaseQuery.check_task_allowed(
                    int(path_params.task_id), query_params.user_name
                )
            )
            row = result.one()
            if not row:
                return False
        return True

    @staticmethod
    async def get_search_task_messages(
        engine: AsyncEngine,
        path_params: GetSearchMessagesPathParams,
        query_params: GetSearchMessagesQueryParams,
    ) -> dict:
        async with engine.begin() as conn:
            res = await conn.execute(
                DatabaseQuery.get_search_task_messages(
                    int(path_params.task_id), query_params.limit, query_params.from_
                )
            )
        resp = {
            "messages": [],
            "iterator": str(query_params.from_ + query_params.limit),
        }
        rows = res.all()
        for row in rows:
            resp["messages"].append({"text": row["text"], "chat_id": row["chat_id"]})
        return resp
