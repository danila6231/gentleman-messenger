from messenger.bot.bot_responses import BotResponse

from messenger.validation import GetMessagesPathParams, GetMessagesQueryParams


class AlternativeResponse:
    def __init__(self):
        pass

    @staticmethod
    def get_messages(
        path_params: GetMessagesPathParams, query_params: GetMessagesQueryParams
    ) -> dict:
        resp = {
            "messages": [{"text": BotResponse.get_messages(query_params.username)}],
            "iterator": "0",
        }
        return resp
