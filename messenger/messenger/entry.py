import asyncio
from aiohttp import web

from messenger.app import create_app

app = create_app()


def main():
    loop = asyncio.get_event_loop()
    app_loop = loop.run_until_complete(app)
    web.run_app(app_loop)


main()
