from sqlalchemy import select, insert, asc, update
from messenger.features.logger import logger

from messenger.db.schema import (
    chats_table,
    users_table,
    messages_table,
    users_data_table,
    tasks_table,
    retrieved_messages_table,
)


class DatabaseQuery:
    def __init__(self):
        pass

    @staticmethod
    @logger("DatabaseQuery")
    def create_chat(chat_name: str):
        return insert(chats_table).values(chat_name=chat_name)

    @staticmethod
    @logger("DatabaseQuery")
    def add_user_data(user_name: str):
        return insert(users_data_table).values(user_name=user_name)

    @staticmethod
    @logger("DatabaseQuery")
    def add_user(user_name: str, chat_id: int):
        return insert(users_table).values(user_name=user_name, chat_id=chat_id)

    @staticmethod
    @logger("DatabaseQuery")
    def get_messages(chat_id: int, limit: int, from_: int):
        return (
            select(messages_table)
            .where(
                (messages_table.c.message_id >= from_)
                & (messages_table.c.chat_id == chat_id)
            )
            .limit(limit)
        )

    @staticmethod
    @logger("DatabaseQuery")
    def send_message(user_id: int, chat_id: int, message: str):
        return insert(messages_table).values(
            text=message, chat_id=chat_id, user_id=user_id
        )

    @staticmethod
    @logger("DatabaseQuery")
    def check_chat_exists(chat_id: int):
        return select(chats_table).where(chats_table.c.chat_id == chat_id)

    @staticmethod
    @logger("DatabaseQuery")
    def check_user_in_chat_exists(chat_id: int, user_id: int):
        return select(users_table).where(
            users_table.c.user_id == user_id and users_table.c.chat_id == chat_id
        )

    @staticmethod
    @logger("DatabaseQuery")
    def add_task(user_name: str, query: str):
        return insert(tasks_table).values(user_name=user_name, query=query)

    @staticmethod
    @logger("DatabaseQuery")
    def check_task_exists(task_id: int):
        return select(tasks_table).where(tasks_table.c.task_id == task_id)

    @staticmethod
    @logger("DatabaseQuery")
    def check_user_exists(user_name: str):
        return select(users_data_table).where(users_data_table.c.user_name == user_name)

    @staticmethod
    @logger("DatabaseQuery")
    def check_task_allowed(task_id: int, user_name: str):
        return select(tasks_table).where(
            (tasks_table.c.task_id == task_id) & (tasks_table.c.user_name == user_name)
        )

    @staticmethod
    @logger("DatabaseQuery")
    def get_search_task_messages(task_id: int, limit: int, from_: int):
        return (
            retrieved_messages_table.select()
            .where(
                (retrieved_messages_table.c.task_id == task_id)
                & (retrieved_messages_table.c.message_id >= from_)
            )
            .limit(limit)
            .order_by(asc(retrieved_messages_table.c.message_id))
        )

    @staticmethod
    @logger("DatabaseQuery")
    def update_task_status(task_id: int, status: str):
        return (
            update(tasks_table)
            .where(tasks_table.c.task_id == task_id)
            .values(status=status)
        )

    @staticmethod
    @logger("DatabaseQuery")
    def process_task(task_id: str, query: str):
        return f"""
            insert into retrieved_messages ( task_id, message_id, text, chat_id )
            select {task_id}, messages.message_id, messages.text, messages.chat_id
            from messages
            where messages.text like '%{query}%';
        """
