from sqlalchemy import (
    Column,
    ForeignKey,
    MetaData,
    Text,
    Table,
    BigInteger,
    Integer,
    TIMESTAMP,
)
from sqlalchemy.sql import functions
from messenger.db.convention import convention

metadata = MetaData(naming_convention=convention)

chats_table = Table(
    "chats",
    metadata,
    Column("chat_id", Integer, primary_key=True),
    Column("chat_name", Text, nullable=False),
    Column("created_time", TIMESTAMP, nullable=False, server_default=functions.now()),
)

users_table = Table(
    "users",
    metadata,
    Column("user_id", Integer, primary_key=True),
    Column("chat_id", Integer, ForeignKey("chats.chat_id"), nullable=False),
    Column("user_name", Text, ForeignKey("users_data.user_name"), nullable=False),
    Column("created_time", TIMESTAMP, nullable=False, server_default=functions.now()),
)

users_data_table = Table(
    "users_data",
    metadata,
    Column("user_id", Integer, primary_key=True),
    Column("user_name", Text, nullable=False, unique=True),
    Column("first_name", Text, nullable=True),
    Column("last_name", Text, nullable=True),
    Column("created_time", TIMESTAMP, nullable=False, server_default=functions.now()),
)

messages_table = Table(
    "messages",
    metadata,
    Column("message_id", BigInteger, primary_key=True),
    Column("chat_id", Integer, ForeignKey("chats.chat_id"), nullable=False),
    Column("user_id", Integer, ForeignKey("users.user_id"), nullable=False),
    Column("text", Text, nullable=False),
    Column("created_time", TIMESTAMP, nullable=False, server_default=functions.now()),
)


tasks_table = Table(
    "tasks",
    metadata,
    Column("task_id", Integer, primary_key=True),
    Column(
        "user_name",
        Text,
        ForeignKey("users_data.user_name"),
        nullable=False,
        index=True,
    ),
    Column("query", Text, nullable=False),
    Column("status", Text, nullable=False, default="Pending"),
)

retrieved_messages_table = Table(
    "retrieved_messages",
    metadata,
    Column("task_id", Integer, ForeignKey("tasks.task_id"), primary_key=True),
    Column("message_id", ForeignKey("messages.message_id"), primary_key=True),
    Column("text", Text, nullable=False),
    Column("chat_id", Integer, ForeignKey("chats.chat_id"), nullable=False, index=True),
)
