import os
from alembic.config import CommandLine, Config
from pathlib import Path
from messenger.constants import POSTGRES_URL

PROJECT_PATH = Path(__file__).parent.parent.resolve()
PG_URL = POSTGRES_URL


def main():
    alembic = CommandLine()
    alembic.parser.add_argument(
        "--pg-url",
        default=os.getenv("PG_URL", PG_URL),
        help="Database URL [env var: PG_URL]",
    )
    options = alembic.parser.parse_args()
    if not os.path.isabs(options.config):
        options.config = os.path.join(PROJECT_PATH, options.config)

    config = Config(file_=options.config, ini_section=options.name, cmd_opts=options)

    alembic_location = config.get_main_option("script_location")
    if not os.path.isabs(alembic_location):
        config.set_main_option(
            "script_location", os.path.join(PROJECT_PATH, alembic_location)
        )

    config.set_main_option("sqlalchemy.url", options.pg_url)

    exit(alembic.run_cmd(config, options))
    return True


if __name__ == "__main__":
    main()
