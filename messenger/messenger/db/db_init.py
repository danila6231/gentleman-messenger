from aiohttp import web
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy import create_engine
from sqlalchemy.pool import QueuePool
from messenger.constants import ASYNC_POSTGRES_URL, POSTGRES_URL
from messenger.db.schema import metadata


def connect_to_db(app_: web.Application):
    if app_["engine"] is not None:
        return
    sync_engine = create_engine(POSTGRES_URL)
    metadata.create_all(sync_engine)
    sync_engine.dispose()
    app_["engine"] = create_async_engine(
        ASYNC_POSTGRES_URL, pool_size=20, poolclass=QueuePool, echo=True
    )
