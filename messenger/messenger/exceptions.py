class ChatNotFound(Exception):
    def __init__(self, message=""):
        self.message = message


class UserInChatNotFound(Exception):
    def __init__(self, message=""):
        self.message = message


class UserNotFound(Exception):
    def __init__(self, message=""):
        self.message = message


class ForbiddenTask(Exception):
    def __init__(self, message=""):
        self.message = message


class TaskNotFound(Exception):
    def __init__(self, message=""):
        self.message = message


class TooManyRequests(Exception):
    def __init__(self, message=""):
        self.message = message


class DatabaseConnectionError(Exception):
    def __init__(self, message=""):
        self.message = message
