from pydantic import BaseModel, typing, Field


class CreateChatBody(BaseModel):
    chat_name: str


class CreateChatQueryParams(BaseModel):
    username: typing.Optional[str] = ""


class AddUserPathParams(BaseModel):
    chat_id: str


class AddUserBody(BaseModel):
    user_name: str


class AddUserQueryParams(BaseModel):
    username: typing.Optional[str] = ""


class GetMessagesPathParams(BaseModel):
    chat_id: str


class GetMessagesQueryParams(BaseModel):
    limit: int
    from_: typing.Optional[int] = Field(0, alias="from")
    username: typing.Optional[str] = ""


class SendMessagePathParams(BaseModel):
    chat_id: str


class SendMessageQueryParams(BaseModel):
    user_id: str
    username: typing.Optional[str] = ""


class SendMessageBody(BaseModel):
    message: str


class StartSearchBody(BaseModel):
    message: str


class StartSearchQueryParams(BaseModel):
    username: typing.Optional[str] = ""


class SearchStatusPathParams(BaseModel):
    task_id: str


class SearchStatusQueryParams(BaseModel):
    user_name: str


class GetSearchMessagesPathParams(BaseModel):
    task_id: str


class GetSearchMessagesQueryParams(BaseModel):
    limit: int
    from_: typing.Optional[int] = Field(0, alias="from")
    user_name: typing.Optional[str] = ""


class Task(BaseModel):
    task_id: str
    query: str
    user_name: str
