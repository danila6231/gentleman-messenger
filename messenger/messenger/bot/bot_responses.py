import csv

import datetime
import os
from pathlib import Path

PROJECT_PATH = Path(__file__).parent.parent.resolve()
settings_file = os.path.join(PROJECT_PATH, "settings.csv")


def time_sensitive_text() -> str:
    time = datetime.datetime.now()
    if (time.hour > 4) and (time.hour <= 12):
        return "Доброе утро, "
    elif (time.hour > 12) and (time.hour <= 17):
        return "Добрый день, "
    elif (time.hour > 17) and (time.hour <= 22):
        return "Добрый вечер, "
    elif (time.hour > 22) and (time.hour <= 4):
        return "Доброй ночи, "


def get_settings_csv(username: str) -> dict:
    csv_file = csv.reader(open(settings_file, "r"), delimiter=",")
    found = False
    for record in csv_file:
        if record[1] == username:
            found = True
            return {"name": record[4]}
    if not found:
        return {"name": ""}


def welcome_user_text(username: str) -> str:
    if username == "":
        return "Доброго времени суток, человек\n"
    user_settings = get_settings_csv(username)
    return time_sensitive_text() + user_settings["name"] + "\n"


class BotResponse:
    @staticmethod
    def get_messages(username: str) -> str:
        return welcome_user_text(username) + "Не удалось подключиться к базе данных.\n"
