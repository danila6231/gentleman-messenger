import datetime
import os.path
from pathlib import Path

PROJECT_PATH = Path(__file__).parent.parent.resolve()
log_file = os.path.join(PROJECT_PATH, "logs.txt")
logger_init = False


def logger_init():
    with open(log_file, "w") as f:
        f.truncate(0)


def logger(classname: str):
    if not logger_init:
        logger_init()

    def decorator(func):
        def wrapped(*args, **kwargs):
            start_time = datetime.datetime.now()
            result = func(*args, **kwargs)
            time_delta = datetime.datetime.now() - start_time
            print(f"{classname}: {func.__name__}\n")
            print(f"{str(time_delta.microseconds // 1000)} ms\n")
            return result

        return wrapped

    return decorator


def log(string: str):
    print(string + "\n")
