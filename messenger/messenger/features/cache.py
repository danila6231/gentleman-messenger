from messenger.validation import GetMessagesPathParams, GetMessagesQueryParams


class CachedResponse:
    def __init__(self, limit: int, from_: int, data: dict):
        self.limit = limit
        self.from_ = from_
        self.data = data


def get_messages_cached(
    cache: dict,
    path_params: GetMessagesPathParams,
    query_params: GetMessagesQueryParams,
):
    chat_id = path_params.chat_id
    limit = query_params.limit
    from_ = query_params.from_
    messages = []
    if chat_id in cache.keys():
        if (
            cache[chat_id].from_ <= from_
            and from_ + limit <= cache[chat_id].from_ + cache[chat_id].limit
        ):
            left = from_ - cache[chat_id].from_
            right = from_ - cache[chat_id].from_ + limit
            messages = cache[chat_id].data["messages"][left:right]
    if len(messages) > 0:
        return {"messages": messages, "iterator": str(from_ + limit)}
    return None


def cache_response(
    cache: dict,
    path_params: GetMessagesPathParams,
    query_params: GetMessagesQueryParams,
    resp: dict,
):
    chat_id = path_params.chat_id
    limit = query_params.limit
    from_ = query_params.from_
    cache[chat_id] = CachedResponse(limit, from_, resp)


def invalidate_cache(cache: dict, chat_id: str):
    if chat_id in cache.keys():
        del cache[chat_id]
