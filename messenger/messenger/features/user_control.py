from datetime import datetime

from messenger.exceptions import TooManyRequests

allowed_time = 5


def user_control(users: dict, username: str) -> None:
    if username == "":
        return
    if (
        username not in users.keys()
        or (datetime.now() - users[username]).seconds >= allowed_time
    ):
        users[username] = datetime.now()
        return
    raise TooManyRequests
