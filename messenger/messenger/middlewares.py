import sys

from aiohttp import web
from aiohttp.web_exceptions import HTTPMethodNotAllowed, HTTPNotFound
from pydantic import ValidationError
from messenger.features.logger import log
from messenger.exceptions import (
    ChatNotFound,
    UserInChatNotFound,
    TooManyRequests,
    DatabaseConnectionError,
    UserNotFound,
    TaskNotFound,
    ForbiddenTask,
)


@web.middleware
async def middleware(request, handler):
    try:
        return await handler(request)
    except ValidationError:
        return web.json_response({"message": "bad-parameters"}, status=400)
    except ForbiddenTask:
        return web.json_response({"message": "forbidden-task"}, status=403)
    except ChatNotFound:
        return web.json_response({"message": "chat-not-found"}, status=404)
    except UserInChatNotFound:
        return web.json_response({"message": "user-in-chat-not-found"}, status=404)
    except UserNotFound:
        return web.json_response({"message": "user-not-found"}, status=404)
    except TaskNotFound:
        return web.json_response({"message": "task-not-found"}, status=404)
    except TooManyRequests:
        return web.json_response({"message": "too-many-requests"}, status=429)
    except (HTTPMethodNotAllowed, HTTPNotFound):
        return web.json_response({"message": "method-not-allowed"}, status=405)
    except DatabaseConnectionError:
        return web.json_response({"message": "Database Connection Error"}, status=503)
    except Exception as e:
        log(repr(e))
        exception_type, exception_object, exception_traceback = sys.exc_info()
        filename = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        log(f"Exception type: {exception_type}")
        log(f"File name: {filename}")
        log(f"Line number: {line_number}")
        return web.json_response({"message": "Internal Server Error"}, status=500)
