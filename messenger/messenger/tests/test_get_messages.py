import pytest
from aiohttp import web
from ..handlers.handler import Handler
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy import create_engine
import testing.postgresql
from ..db.schema import metadata
from ..middlewares import middleware
import json
from ..tests.utils import (
    make_create_chat_request,
    make_send_message_request,
    make_get_message_request,
    make_add_user_request,
)


@pytest.mark.asyncio
async def test_get_messages(
    add_user_request_text,
    create_chat_request_text,
    send_message_request_text,
):

    with testing.postgresql.Postgresql() as test_db:
        db_url = "postgresql+asyncpg" + test_db.url()[10:]
        engine = create_async_engine(db_url)
        async with engine.begin() as conn:
            await conn.run_sync(metadata.create_all)
        request = make_get_message_request(engine=engine)
        resp: web.Response = await middleware(request, Handler.get_messages)
        assert resp.status == 404

        create_chat_request = make_create_chat_request(
            body_text=create_chat_request_text, engine=engine
        )
        resp: web.Response = await middleware(create_chat_request, Handler.create_chat)
        assert resp.status == 201

        add_user_request = make_add_user_request(
            body_text=add_user_request_text, engine=engine
        )
        resp: web.Response = await middleware(add_user_request, Handler.add_user)
        assert resp.status == 201

        send_message_request = make_send_message_request(
            body_text=send_message_request_text, engine=engine
        )
        resp: web.Response = await middleware(
            send_message_request, Handler.send_message
        )
        assert resp.status == 201

        resp: web.Response = await middleware(request, Handler.get_messages)
        assert resp.status == 200
        correct_resp = {
            "messages": [{"text": "Hello"}],
            "iterator": "10",
        }
        assert json.loads(resp.body.decode()) == correct_resp
