import pytest
from aiohttp import web
from ..handlers.handler import Handler
from sqlalchemy.ext.asyncio import create_async_engine
from unittest.mock import Mock
import testing.postgresql
from ..middlewares import middleware


@pytest.mark.asyncio
async def test_ping_db():
    request = Mock()
    with testing.postgresql.Postgresql() as test_db:
        db_url = "postgresql+asyncpg" + test_db.url()[10:]
        request.app = {"engine": create_async_engine(db_url)}
        resp: web.Response = await middleware(request, Handler.ping_db)
        assert resp.status == 200
    request.app = {"engine": create_async_engine(db_url)}
    resp = await middleware(request, Handler.ping_db)
    assert resp.status == 503
