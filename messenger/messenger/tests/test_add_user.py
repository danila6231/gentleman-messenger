import pytest
from aiohttp import web
from ..handlers.handler import Handler
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy import create_engine
import testing.postgresql
from ..db.schema import metadata
from ..middlewares import middleware
from ..tests.utils import make_create_chat_request, make_add_user_request


@pytest.mark.asyncio
async def test_add_user(add_user_request_text, create_chat_request_text):

    with testing.postgresql.Postgresql() as test_db:
        db_url = "postgresql+asyncpg" + test_db.url()[10:]
        engine = create_async_engine(db_url)
        async with engine.begin() as conn:
            await conn.run_sync(metadata.create_all)
        request = make_add_user_request(body_text=add_user_request_text, engine=engine)
        resp: web.Response = await middleware(request, Handler.add_user)
        assert resp.status == 404

        create_chat_request = make_create_chat_request(
            body_text=create_chat_request_text, engine=engine
        )
        resp: web.Response = await middleware(create_chat_request, Handler.create_chat)
        assert resp.status == 201
        resp: web.Response = await middleware(request, Handler.add_user)
        assert resp.status == 201
