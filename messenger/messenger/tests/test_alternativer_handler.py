from ..handlers.alternative_handler import AlternativeResponse


def test_alternative_handler(get_messages_path_params, get_messages_query_params):
    resp = AlternativeResponse.get_messages(
        get_messages_path_params, get_messages_query_params
    )
    correct_resp = {
        "messages": [
            {
                "text": "Доброго времени суток, человек\n"
                "Не удалось подключиться к базе данных.\n"
            }
        ],
        "iterator": "0",
    }
    assert resp == correct_resp
