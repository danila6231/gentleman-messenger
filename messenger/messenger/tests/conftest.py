import os
from pathlib import Path
from ..validation import GetMessagesPathParams, GetMessagesQueryParams
import pytest
import yaml


@pytest.fixture(scope="session")
def get_messages_path_params():
    return GetMessagesPathParams.parse_obj({"chat_id": "1"})


@pytest.fixture(scope="session")
def get_messages_query_params():
    return GetMessagesQueryParams.parse_obj({"limit": 10, "from": 0, "username": ""})


@pytest.fixture(scope="session")
def create_chat_request_text():
    return """{
                "chat_name": "new chat"
              }"""


@pytest.fixture(scope="session")
def send_message_request_text():
    return """{
                "message": "Hello"
              }"""


@pytest.fixture(scope="session")
def add_user_request_text():
    return """{
                "user_name": "michael1234"
              }"""


@pytest.fixture(scope="session")
def base_url():
    return os.getenv("MESSENGER_URL", "http://127.0.0.1:8080")


@pytest.fixture(scope="session")
def chat_id():
    return "123"


@pytest.fixture(scope="session")
def get_spec():
    spec_path = Path(__file__).parent.parent / "swagger.yml"
    with open(spec_path) as f:
        data = yaml.safe_load(f)

    def get_spec_segment(*path_segments):
        item = data
        for path_segment in path_segments:
            item = item.__getitem__(path_segment)
        return item

    return get_spec_segment


@pytest.fixture(scope="session")
def error_response_schema(get_spec):
    return get_spec(
        "components",
        "responses",
        "DefaultErrorResponse",
        "content",
        "application/json",
        "schema",
    )
