from ..bot.bot_responses import get_settings_csv


def test_get_settings_csv():
    assert get_settings_csv("danila6231") == {"name": "Danila"}
    assert get_settings_csv("sfasfaf") == {"name": ""}
