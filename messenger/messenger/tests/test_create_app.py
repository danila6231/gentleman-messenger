import pytest

from ..app import create_app


@pytest.mark.asyncio
async def test_create_app():
    app = await create_app()
    assert app is not None
