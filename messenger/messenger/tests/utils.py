import asyncio
from unittest.mock import MagicMock, patch


def check_headers(headers):
    content_type = headers.get("content-type")
    assert content_type and "application/json" in content_type


def async_return(result):
    f = asyncio.Future()
    f.set_result(result)
    return f


@patch("aiohttp.web.Request")
def make_create_chat_request(mock_request, body_text, engine=None):
    print(mock_request)
    request = mock_request.return_value
    request.text = MagicMock(return_value=async_return(body_text))
    request.app = {"engine": engine, "user_control": dict(), "cache": dict()}
    request.query = {"username": ""}
    return request


@patch("aiohttp.web.Request")
def make_add_user_request(mock_request, body_text, chat_id=1, engine=None):
    request = mock_request.return_value
    request.text = MagicMock(return_value=async_return(body_text))
    request.app = {"engine": engine, "user_control": dict(), "cache": dict()}
    request.query = {"username": ""}
    request.match_info = {"chat_id": str(chat_id)}
    return request


@patch("aiohttp.web.Request")
def make_send_message_request(
    mock_request, body_text, chat_id=1, user_id=1, engine=None
):
    request = mock_request.return_value
    request.text = MagicMock(return_value=async_return(body_text))
    request.app = {"engine": engine, "user_control": dict(), "cache": dict()}
    request.query = {"username": "", "user_id": str(user_id)}
    request.match_info = {"chat_id": str(chat_id)}
    return request


@patch("aiohttp.web.Request")
def make_get_message_request(mock_request, chat_id=1, limit=10, from_=0, engine=None):
    request = mock_request.return_value
    request.app = {"engine": engine, "user_control": dict(), "cache": dict()}
    request.query = {"username": "", "limit": limit, "from": from_}
    request.match_info = {"chat_id": str(chat_id)}
    return request
