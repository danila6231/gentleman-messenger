from ..features.cache import get_messages_cached, cache_response
from ..features.user_control import user_control
from ..validation import GetMessagesPathParams, GetMessagesQueryParams
from ..exceptions import TooManyRequests


def test_cache(get_messages_path_params, get_messages_query_params):
    path_params = get_messages_path_params
    query_params = get_messages_query_params
    cache = dict()
    assert get_messages_cached(cache, path_params, query_params) is None

    resp = {"messages": [], "iterator": str(10)}
    for i in range(10):
        resp["messages"].append({"text", str(i)})
    cache_response(cache, path_params, query_params, resp)

    path_params = GetMessagesPathParams.parse_obj({"chat_id": "1"})
    query_params = GetMessagesQueryParams.parse_obj(
        {"limit": 4, "from": 2, "username": ""}
    )
    correct_resp = {"messages": [], "iterator": str(6)}
    for i in range(2, 6):
        correct_resp["messages"].append({"text", str(i)})
    assert get_messages_cached(cache, path_params, query_params) == correct_resp


def test_user_control():
    users = dict()
    assert user_control(users, "Mike") is None
    exception_caught = False
    try:
        user_control(users, "Mike")
    except TooManyRequests:
        exception_caught = True
    assert exception_caught
