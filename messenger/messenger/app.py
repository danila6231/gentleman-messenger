from aiohttp import web

from messenger.handlers.handler import Handler

from messenger.features.logger import logger_init

from messenger.middlewares import middleware


async def create_app():
    handler = Handler()
    routes = [
        web.post("/v1/chats", handler.create_chat),
        web.post("/v1/chats/{chat_id}/users", handler.add_user),
        web.get("/v1/chats/{chat_id}/messages", handler.get_messages),
        web.post("/v1/chats/{chat_id}/messages", handler.send_message),
        web.get("/ping_db", handler.ping_db),
        web.get("/ping", handler.ping),
    ]
    app = web.Application(middlewares=[middleware])
    app.add_routes(routes)
    app["engine"] = None
    app["cache"] = dict()
    app["user_control"] = dict()
    logger_init()
    return app
