# gentleman-messenger

This is a backend messenger, swagger.yml contains a description of all handles

The deployment was done locally using ansible-playbook using the following command:
```
ansible-playbook -i hosts.ini playbook.yml
```

The domain used is danila6231.me (currently dead)

Commands to test handles:

```
curl --location --request GET 'http://danila6231.me/ping'
```
```
curl --location --request GET 'http://danila6231.me/ping_db'
```
```
curl --location --request POST 'http://danila6231.me/v1/chats' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "chat_name": "new_chat"
}'
```
```
curl --location --request POST 'http://danila6231.me/v1/chats/1/users' \
--header 'Content-Type: application/json' \
--data-raw '{
  "user_name": "Vasya Pupkin"
}'
```
```
curl --location --request POST 'http://danila6231.me/v1/chats/1/messages?user_id=1' \
--header 'Content-Type: text/plain' \
--data-raw '{
  "message": "Hello"
}'
```
```
curl --location --request GET 'http://danila6231.me/v1/chats/1/messages?limit=10'
```