FROM snakepacker/python:all as builder

RUN python3.9 -m venv /usr/share/python3/app
RUN /usr/share/python3/app/bin/pip install -U pip

COPY . mnt/
WORKDIR /mnt/

RUN apt-get update && apt-get install -y postgresql
RUN /usr/share/python3/app/bin/pip install -U -r messenger/requirements.txt

RUN /usr/share/python3/app/bin/pip install ./messenger

FROM snakepacker/python:3.9 as messenger

COPY --from=builder /usr/share/python3/app /usr/share/python3/app
COPY . mnt/
WORKDIR /mnt/

RUN ln -snf /usr/share/python3/app/bin/messenger-api /usr/local/bin/

CMD ["messenger-api"]
